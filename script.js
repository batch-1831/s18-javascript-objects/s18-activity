let trainer = {};

// trainer properties
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
}
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.talk = function() {
    console.log("Pikachu! I choose you!");
}

// Pokemon constructor and methods
function Pokemon(name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    
    // Method
    this.tackle = function(targetPokemon) {
        console.log(`${this.name} tackled ${targetPokemon.name}`);
        
        // newHealth = targetHealth - pokemonAttack
        let newTargetPokemonHealth = targetPokemon.health - this.attack;

        console.log(`${targetPokemon.name}'s health is reduced to ${newTargetPokemonHealth}`);

        // call faint method if the target pokemon's health is less than or equal to zero
        if (newTargetPokemonHealth <= 0) {
            targetPokemon.faint();
        }
        // update the target pokemon's health
        else {
            targetPokemon.health = newTargetPokemonHealth;
        }

        // show target pokemon current stats
        console.log(targetPokemon);
   }

   this.faint = function() {
       console.log(`${this.name} fainted`);
   }
}

// Create Pokemons
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

// results
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);


console.log("Result of talk method:");
trainer.talk();

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

// battle
geodude.tackle(pikachu);
mewtwo.tackle(geodude);
